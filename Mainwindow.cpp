#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QDebug>
#include <QJsonDocument>

MainWindow *handle;
float CPUTemp;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->CPUTemp=10;
    this->time = 0;
    this->Pitch=0;
    this->Roll=0;
    this->setWindowTitle("EE513 Assignment 2");
    this->ui->customPlot->addGraph();
    this->ui->customPlot->yAxis->setLabel("Temp ");
    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    QSharedPointer<QCPAxisTickerTime> timeTicker1(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%h:%m:%s");
    this->ui->customPlot->xAxis->setTicker(timeTicker);
    this->ui->customPlot->yAxis->setRange(30,50);
    this->ui->customPlot->addGraph();
    this->ui->customPlot->yAxis->setLabel("Temp");

    timeTicker1->setTimeFormat("%h:%m:%s");
    this->ui->customPlot->xAxis->setTicker(timeTicker1);
    this->ui->customPlot->yAxis->setRange(30,50);
    this->ui->customPlot->replot();
    QObject::connect(this, SIGNAL(messageSignal(QString)),
                     this, SLOT(on_MQTTmessage(QString)));
    ::handle = this;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::update(){
    // For more help on real-time plots, see: http://www.qcustomplot.com/index.php/demos/realtimedatademo
    static QTime time(QTime::currentTime());
    double key = time.elapsed()/1000.0; // time elapsed since start of demo, in seconds
    ui->customPlot->graph(0)->addData(key,CPUTemp);
    ui->customPlot->graph(0)->rescaleKeyAxis(true);
   ui->customPlot->replot();
    QString text = QString("Value added is %1").arg(this->CPUTemp);
    //static QTime time(QTime::currentTime());
    //double key = time.elapsed()/1000.0; // time elapsed since start of demo, in seconds
   //ui->customPlot->graph(1)->addData(key,Roll);
  // ui->customPlot->graph(1)->rescaleKeyAxis(true);
    //ui->customPlot->replot();
    //QString text1 = QString("Value added is %1").arg(this->Roll);
    ui->outputEdit->setText(text);
}

void MainWindow::on_downButton_clicked()
{
    this->CPUTemp-=0.62;
    this->Roll+=0;
    this->Pitch-=0;
    this->update();
}

void MainWindow::on_upButton_clicked()
{
    this->CPUTemp+=0.29;
    this->Roll+=0;
    this->Pitch+=0;
    this->update();
}
void MainWindow::on_connectButton_clicked()
{
    MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
    int rc;
    MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    opts.keepAliveInterval = 20;
    opts.cleansession = 1;
    opts.username = AUTHMETHOD;
    opts.password = AUTHTOKEN;

    if (MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered)==0){
        ui->outputText->appendPlainText(QString("Callbacks set correctly"));
    }
    if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) {
        ui->outputText->appendPlainText(QString("Failed to connect, return code %1").arg(rc));
    }
    ui->outputText->appendPlainText(QString("Subscribing to topic " TOPIC " for client " CLIENTID));
    int x = MQTTClient_subscribe(client, TOPIC, QOS);
    ui->outputText->appendPlainText(QString("Result of subscribe is %1 (0=success)").arg(x));
}

void delivered(void *context, MQTTClient_deliveryToken dt) {
    (void)context;
    // Please don't modify the Window UI from here
    qDebug() << "Message delivery confirmed";
    handle->deliveredtoken = dt;
}

/* This is a callback function and is essentially another thread. Do not modify the
 * main window UI from here as it will cause problems. Please see the Slot method that
 * is directly below this function. To ensure that this method is thread safe I had to
 * get it to emit a signal which is received by the slot method below */
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message) {
    (void)context; (void)topicLen;
    qDebug() << "Message arrived (topic is " << topicName << ")";
    qDebug() << "Message payload length is " << message->payloadlen;

    QString payload;
    payload.sprintf("%s", (char *) message->payload).truncate(message->payloadlen);
    emit handle->messageSignal(payload);

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

/** This is the slot method. Do all of your message received work here. It is also safe
 * to call other methods on the object from this point in the code */
void MainWindow::on_MQTTmessage(QString payload){
    ui->outputText->appendPlainText(payload);
    ui->outputText->ensureCursorVisible();
    QJsonDocument doc = QJsonDocument::fromJson(payload.toUtf8());
    QJsonObject obj = doc.object();
    QJsonObject sample = obj["d"].toObject();
    this->Pitch = (float) sample["Pitch"].toDouble();
    this->Roll = (float) sample["Roll"].toDouble();
    this->CPUTemp = (float) sample["CPUTemp"].toDouble();
    //this->humidity = (float) sample["humidity"].toDouble();
 //   cout << "The temperature is " << temperature  " and humidity is "
//         << humidity << endl;

    //ADD YOUR CODE HERE
}
//int MainWindow::parseJSONData(QString str){

//  return 0;
//}

void connlost(void *context, char *cause) {
    (void)context; (void)*cause;
    // Please don't modify the Window UI from here
    qDebug() << "Connection Lost" << endl;
}

void MainWindow::on_disconnectButton_clicked()
{
    qDebug() << "Disconnecting from the broker" << endl;
    MQTTClient_disconnect(client, 10000);
    //MQTTClient_destroy(&client);
}


